package kr.netty;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClHandlerTest {
    @Test
    public void testInput(){
        String s="Москва",s1="утро",str="";
        str = s + ":" + s1;

        String[] test = str.split(":");
        assertEquals(2,test.length);
        assertFalse(test[0].equals("quit"));
        assertTrue(test[0].equals(s));
        assertTrue(test[1].equals(s1));
    }
}
