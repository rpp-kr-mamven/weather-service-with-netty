package kr.netty;

import java.util.Scanner;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import io.netty.util.ReferenceCountUtil;
import io.netty.buffer.Unpooled;

import io.netty.util.concurrent.BlockingOperationException;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.*;

public class ClientHandler extends ChannelInboundHandlerAdapter
{
	static String s,s1,str;
	@Override
	public void channelActive(final ChannelHandlerContext ctx)
	{
		Scanner scan = new Scanner(System.in);
		System.out.print("Введите название города\n");
		s = scan.nextLine();
		if(s.equals("quit")){
			System.out.println("завершение работы программы.....");
		}
		else {
			do {
				System.out.println("Введите время суток(утро, день, вечер, ночь, сутки (вся погода))");
				s1 = scan.nextLine();
			}while (!((s1.equalsIgnoreCase("сутки"))||(s1.equalsIgnoreCase("утро")) || (s1.equalsIgnoreCase("день")) || (s1.equalsIgnoreCase("вечер")) || (s1.equalsIgnoreCase("ночь"))));
		}
		str = s + ":" + s1;
		final ByteBuf out = Unpooled.wrappedBuffer(str.getBytes());
		ctx.writeAndFlush(out);
	}
	
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
	{
		ByteBuf in = (ByteBuf) msg;
		String str;
		try
		{
			str = in.toString(Charset.forName("utf-8"));
			if(str.equals("Выход")){
				System.out.println("Спасибо за пользование программой :)");
			}else{
				System.out.println("Погода на " + s1 + ": " + str+"\n\n");
			}

		}
		finally
		{
			ReferenceCountUtil.release(msg);
		}
	}
	
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	{
        cause.printStackTrace();
        ctx.close();
    }
}
