package kr.netty;

import java.sql.SQLException;


public class TaskExecutor implements Runnable
{
	private CQueue qIn = new CQueue(50);
	private CQueue qOut = new CQueue(50);
	
	private boolean isThreadActive;

	
	public TaskExecutor(CQueue qIn, CQueue qOut)
	{
		this.qIn=qIn;
		this.qOut=qOut;
		this.isThreadActive = true;
	}
	
	@Override
	public void run()
	{
		while (isThreadActive)
		{
			Task curTask = null;
			curTask = qIn.get();
			if (curTask!=null){
				boolean ignore=true;
				for(String city :DataBase.Weather.getCities()){
					if(curTask.city.equals(city)){
						ignore=false;
					}
				}
				if(ignore==false){
					try{
						curTask.temperature=DataBase.Weather.getWeather(curTask.city, curTask.type);
					}catch(SQLException e){
						e.printStackTrace();
					}
				}else{
					if(curTask.city.equals("quit")){
						curTask.temperature="Выход";
					}else{
						curTask.temperature="Введенный город отсутствует. Информации нет";
					}
				}
				qOut.add(curTask);
			}
			try{
				Thread.sleep(100);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	public void close()
	{
		isThreadActive = false;
	}
}