package kr.netty;

import io.netty.channel.ChannelHandlerContext;

import java.util.Date;

public class Task
{
	public int id;
	public String city;
	public String temperature;
	public String type;
	public ChannelHandlerContext ctx;

	public Task(int id,String city, String type, ChannelHandlerContext ctx)
	{
		this.id=id;
		this.city=city;
		this.type = type;
		this.temperature="0";
		this.ctx=ctx;
	}
}