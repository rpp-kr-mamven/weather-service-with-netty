package kr.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.*;

public class Logger implements Runnable
{
	private CQueue qOut = new CQueue(50);
	
	private boolean isThreadActive;
	
	public Logger(CQueue qOut)
	{
		this.qOut=qOut;
		this.isThreadActive = true;
	}
	
	@Override
	public void run()
	{
		while (isThreadActive)
		{
			Task curTask = null;
			curTask = qOut.get();
			if (curTask!=null)
			{
				ChannelHandlerContext ctx = curTask.ctx;
				if(!curTask.city.equals("quit")){
					System.out.println("Отправка текущей погоды по запросу: "+curTask.temperature);
				}
				ByteBuf out = Unpooled.wrappedBuffer(curTask.temperature.getBytes());
				final ChannelFuture f = ctx.writeAndFlush(out);
				f.addListener(new ChannelFutureListener()
				{
					@Override
					public void operationComplete(ChannelFuture future)
					{
						assert f == future;
						ctx.close();
					}
				});
			}
			else
				try{Thread.sleep(100);}catch (InterruptedException e){/*e.printStackTrace();*/}
		}
	}
	
	public void close()
	{
		isThreadActive = false;
	}
}