package kr.netty;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public enum DataBase {
    Weather();
    private Connection conn;

    DataBase(){
        try {
            conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
            prepareDatabase();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareDatabase() throws SQLException {

        PreparedStatement ps = conn.prepareStatement("DELETE FROM weatherF");
        ps.execute();
        ps = conn.prepareStatement("INSERT INTO weatherF VALUES ('Москва', '+10:+13:+9:+5'), ('Тверь','+27:+30:+20:+17'), ('Сочи', '+15:+23:+20:+17'), ('Краснодар', '+20:+29:+26:+20'), ('Минск','+20:+22:+17:+14'), ('Саратов','+10:+16:+13:+7')");      ps.execute();
        ps = conn.prepareStatement("SELECT * from weatherF");

    }

    public void closeConnection() throws SQLException{
        conn.close();
    }

    public String getWeather(String city, String type)throws SQLException{
        String Result;
        String[] tmp;
        synchronized (conn) {
            PreparedStatement ps = conn.prepareStatement("SELECT Weather FROM WeatherF WHERE city ='" + city + "'");
            ResultSet rs = ps.executeQuery();
            rs.beforeFirst();
            rs.next();
            Result = rs.getString("Weather");
            tmp = Result.split(":");
            switch (type){
                case "утро": {Result = tmp[0]; break;}
                case "день": {Result = tmp[1]; break;}
                case "вечер": {Result = tmp[2]; break;}
                case "ночь": {Result = tmp[3]; break;}
                case "сутки": {Result = "Утром "+tmp[0]+", днем "+tmp[1]+", вечером "+tmp[2]+", ночью "+tmp[3]; break;}
            }
            }
        return Result;
    }

    public Set<String> getCities(){
        try {
            Set<String> result = new HashSet<>();
            synchronized (conn) {
                PreparedStatement ps = conn.prepareStatement("SELECT City FROM WeatherF");
                ResultSet rs = ps.executeQuery();
                rs.beforeFirst();
                while (rs.next()) {
                    result.add(rs.getString("City"));
                }
            }
            return  result;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    } }

