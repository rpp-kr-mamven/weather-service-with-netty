package kr.netty;

import io.netty.buffer.ByteBuf;
import io.netty.util.ReferenceCountUtil;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.BlockingQueue;

import java.nio.charset.Charset;
import java.util.Scanner;


public class ServerHandler extends ChannelInboundHandlerAdapter
{
	private CQueue qIn;
	ServerHandler(CQueue qIn)
	{
		this.qIn = qIn;
	}
	
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
		ByteBuf in = (ByteBuf) msg;
		String str;
		String name;
		String[] ask;
		int counter = 0;
		try {
			str = in.toString(Charset.forName("utf-8"));
			Scanner sc = new Scanner(str);
			name = sc.nextLine();
			name = name.replaceAll(" ", "");
			ask = name.split(":");
			if(!ask[0].equalsIgnoreCase("quit")){
				System.out.println("Получен запрос температуры на город: "+ ask[0] + " на " + ask[1]);
			}
			qIn.add(new Task(counter, ask[0], ask[1].toLowerCase(), ctx));
			counter++;
		} finally {
			ReferenceCountUtil.release(msg);
		}
	}


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	{
        cause.printStackTrace();
        ctx.close();
    }
}