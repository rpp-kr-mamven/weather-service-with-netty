package kr.netty;

import org.junit.Test;
import static org.junit.Assert.*;

public class LoggerTest {

   private CQueue qOut;
        int maxSize = 50;

        @Test
        public void runTest() {
            qOut = new CQueue(maxSize);
            Task task = new Task(1,"Москва", "ночь",null);
            task.temperature = "+10";
            Task curTask = new Task(1,"Москва", "ночь",null);
            qOut.add(curTask);

            Logger logger = new Logger(qOut);
            Thread loggerStream = new Thread(logger);

            loggerStream.start();
            loggerStream.interrupt();

            assertTrue(!qOut.isEmpty());

        }
    }