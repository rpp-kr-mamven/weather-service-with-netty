package kr.netty;

import org.junit.Test;
import java.util.LinkedList;
import static org.junit.Assert.*;

public class SrvHandlerTest {

    @Test
    public void ReqTest(){
        String request="Test:day";
        CQueue qIn = new CQueue(50);
        String[] ask = request.split(":");
        assertTrue(request.equals(ask[0]+":"+ask[1]));
        assertEquals(0,qIn.size());
        qIn.add(new Task(0,ask[0], ask[1].toLowerCase(),null));
        assertEquals(1,qIn.size());
        Task test = qIn.get();
        assertTrue(test.city.equals("Test"));
        assertTrue(test.type.equals("day"));
        assertEquals(0,qIn.size());
        assertTrue(qIn.isEmpty());
    }
}
