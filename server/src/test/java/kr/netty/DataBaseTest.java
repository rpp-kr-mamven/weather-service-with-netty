package kr.netty;

import org.junit.Assert;
import org.junit.Test;
import java.util.Set;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class DataBaseTest {

    @Test
    public void getWeather() throws SQLException {
        String result = DataBase.Weather.getWeather("Москва","день");
        assertEquals("+13", result);

    }

    @Test
    public void getCities() throws SQLException {
        Set<String> result = (Set<String>) DataBase.Weather.getCities();
        assertEquals(6,result.size());
        assertTrue(result.contains("Москва"));

    }
}