package kr.netty;

import org.junit.Test;
import static org.junit.Assert.*;

public class CQueueTest {
    Task task = new Task(1, "Город","Сутки",null);
    @Test
    public void pushTest(){
        CQueue test = new CQueue(1);
        assertTrue(test.add(task));
    }
    @Test
    public void popTest() {
        CQueue test = new CQueue(1);
        assertNull(test.get());
        test.add(task);
        assertEquals(task, test.get());
    }
}
