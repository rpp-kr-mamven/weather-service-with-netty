package kr.netty;
import org.junit.Test;
import static org.junit.Assert.*;



public class TaskExecutorTest {
    private CQueue qIn = new CQueue(50);
    private CQueue qOut = new CQueue(50);


    @Test
    public void runTest() {
        Task task = new Task(0, "Тверь","день",null);
        qIn.add(task);
        TaskExecutor taskExecutor = new TaskExecutor(qIn, qOut);
        Thread executorStream = new Thread(taskExecutor);

        executorStream.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {}

        assertTrue(qIn.isEmpty());

        task = qOut.get();
        assertEquals("+30",task.temperature);
        assertEquals(0, task.id);
        assertEquals("Тверь", task.city);
    }
    }
